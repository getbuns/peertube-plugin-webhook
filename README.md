# PeerTube plugin Bunseed Webhook

This plugin enable webhook support :

* when the user click on publish button for a new video


**Be careful:** this plugin is not prod ready. It is currently developped for [https://bunseedorg](Bunseed) for some tests during our next release

For peertube plugin documentation, please see https://docs.joinpeertube.org/#/contribute-plugins?id=write-a-plugintheme


# How to use it

* Go to the plugin admin page on your peertube and edit config for this plugin
* You need to type an URL which be called each time a user click on `publish button` after it uploads a video
* Just before publishing a video, you can disable the call by disable the check box

Webhook need to accept a HTTP `POST` request. The `body` is an `application/json` :

    {
        uuid: UUID
    }
