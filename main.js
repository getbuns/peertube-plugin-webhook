async function register({
    registerHook,
    peertubeHelpers: { logger },
    registerSetting,
    settingsManager
}) {
    registerHook({
        target: 'action:api.video.updated',
        handler: async ({ video, body }) => {
            const doWeNeedToCallWebhook = body.pluginData['bunseed-creat-draft-blogging'];
            if (doWeNeedToCallWebhook !== "true") {
                return;
            }
            logger.info('Bunseed - webHook plugin declenched');
            const webhookUrlInSettings = await settingsManager.getSetting('publish-webhook');
            const request = new Request(webhookUrlInSettings, {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({ uuid: video.uuid })
            });
            const msg = `video uuid=${video.uuid} on webhook url=${webhookUrlInSettings}`;
            try {
                await fetch(request);
                logger.info(`webhook ok for ${msg}`);
            } catch (error) {
                logger.error(`something does wrong during webhook execution ${msg}`);
            }
        }
    });

    registerSetting({
        type: 'html',
        html: '<h3>Webhook settings</h3>'
    });

    registerSetting({
        name: 'publish-webhook',
        type: 'input',
        label: 'Webhook when publish new content',
        private: false
    });
}

async function unregister() {
    return
}

module.exports = {
    register,
    unregister
}
