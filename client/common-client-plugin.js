function register ({ registerVideoField }) {
    const myOptions = {
        name: 'bunseed-creat-draft-blogging',
        label: 'Create draft to bunseed blogging',
        descriptionHTML: '',
        type: 'input-checkbox',
        default: 'true'
    };
    const videosFromOptions = { tab: 'main' };

    for (const type of ['upload', 'import-url', 'import-torrent', 'go-live']) {
        registerVideoField(myOptions, { type, ...videosFromOptions });
        logger.info(type)
    }
}

export {
  register
}
